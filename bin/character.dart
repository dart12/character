abstract class character{
  var name;
  var job;
  var sex;
  var age;

  character(name, job, sex,age){
    this.name=name;
    this.job=job;
    this.sex=sex;
    this.age=age;
  }
}
class atk{
  void normalatk(){}
  void skill(){}
}

class Warrior extends character implements atk {
  Warrior(super.name, super.job, super.sex, super.age);

    @override
  void normalatk() {
    print("ATK MELEE");
  }

  @override
  void skill() {
    print("Call EM.");
  }
}
class Bowman extends character implements atk{
  Bowman(super.name, super.job, super.sex, super.age);

  @override
  void normalatk() {
    print("ATK Range");
  }

  @override
  void skill() {
    print("Double Shot");
  }
}
void main(){
  var Warrior1 = new Warrior("specter", "GrabFood", "LGBTQ+", "21");
  Warrior1.normalatk();
  Warrior1.skill();
  var Bowman1 = new Bowman("Lucy", "Merchant", "F", "31");
  Bowman1.normalatk();
  Bowman1.skill();
}